local tArgs = { ... }
if #tArgs ~= 1 then
	print( "Usage: mine <length>" )
	return
end

local length = tonumber( tArgs[1] )
if length < 1 then
	print( "Tunnel length must be positive" )
	return
end

local depth = 0
local resumeDepth = 0
local collected = 0

local function collect()
	collected = collected + 1
	if math.fmod(collected, 25) == 0 then
		print( "Mined "..collected.." items." )
	end
end

local function tryDig()
	while turtle.detect() do
		if turtle.dig() then
			collect()
			sleep(0.1)
		else
			return false
		end
	end
	return true
end

local function tryDigUp()
	while turtle.detectUp() do
		if turtle.digUp() then
			collect()
			sleep(0.1)
		else
			return false
		end
	end
	return true
end

local function tryDigDown()
	while turtle.detectDown() do
		if turtle.digDown() then
			collect()
			sleep(0.1)
		else
			return false
		end
	end
	return true
end

local function refuel()
	local fuelLevel = turtle.getFuelLevel()
	if fuelLevel == "unlimited" or fuelLevel > 0 then
		return
	end
	
	local function tryRefuel()
		for n=1,16 do
			if turtle.getItemCount(n) > 0 then
				turtle.select(n)
				if turtle.refuel(1) then
					turtle.select(1)
					return true
				end
			end
		end
		turtle.select(1)
		return false
	end
	
	if not tryRefuel() then
		print( "Add more fuel to continue." )
		while not tryRefuel() do
			os.pullEvent( "turtle_inventory" )
		end
		print( "Resuming Tunnel." )
	end
end

local function tryUp()
	refuel()
	while not turtle.up() do
		if turtle.detectUp() then
			if not tryDigUp() then
				return false
			end
		elseif turtle.attackUp() then
			collect()
		else
			sleep( 0.5 )
		end
	end
	return true
end

local function tryDown()
	refuel()
	while not turtle.down() do
		if turtle.detectDown() then
			if not tryDigDown() then
				return false
			end
		elseif turtle.attackDown() then
			collect()
		else
			sleep( 0.5 )
		end
	end
	return true
end

local function tryForward()
	refuel()
	while not turtle.forward() do
		if turtle.detect() then
			if not tryDig() then
				return false
			end
		elseif turtle.attack() then
			collect()
		else
			sleep( 0.5 )
		end
	end
	return true
end

local function isOre(block)
	if block:match('.+_ore$') == nil then
		return false
	else
		return true
	end
end

local function watchAround()
	local success, block = turtle.inspectUp()
	if success and isOre(block.name) then
		tryDigUp()
		tryUp()
		watchAround()
		tryDown()
	end

	success, block = turtle.inspectDown()
	if success and isOre(block.name) then
		tryDigDown()
		tryDown()
		watchAround()
		tryUp()
	end

	for i=1,4 do
		success, block = turtle.inspect()
		if success and isOre(block.name) then
			tryDig()
			tryForward()
			watchAround()
			turtle.turnLeft()
			turtle.turnLeft()
			tryForward()
			turtle.turnRight()
		else
			turtle.turnLeft()
		end
	end
end


local function dropItems()
	for i=2,16 do
		turtle.select(i)
		turtle.dropDown()
	end
	turtle.select(1)
end

local function goBack()
	turtle.turnLeft()
	turtle.turnLeft()
	while depth > 0 do
		if tryForward() then
			depth = depth - 1
		else
			turtle.dig()
		end
	end
	turtle.turnRight()
	turtle.turnRight()
	dropItems()
end

local function resume()
	while depth < resumeDepth do
		if tryForward() then
			depth = depth + 1
		else
			turtle.dig()
		end
	end
end

print( "Tunnelling..." )

for n=1,length do
	tryDigUp()
	watchAround()
	tryUp()
	watchAround()
	tryDown()
	
	if n<length then
		tryDig()
		if not tryForward() then
			print( "Aborting Tunnel." )
			break
		end
		depth = depth + 1
	else
		print( "Tunnel complete." )
	end

	if turtle.getItemCount(16) > 0 then
		turtle.select(1)
		resumeDepth = depth
		goBack()
		resume()
	end
end

print( "Returning to start..." )

-- Return to where we started
goBack()

print( "Tunnel complete." )
print( "Mined "..collected.." items total." )
